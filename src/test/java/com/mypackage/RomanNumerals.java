package com.mypackage;

public class RomanNumerals {


    public static Integer romanNumeralConverter(String rn) {
        String[] rnArray = rn.split("");
        int math = 0;
        int loopCount = 0;
        for (int i = 0; i < rnArray.length; i++) {

            if (i == rnArray.length - 1) {
                if (rnArray[i].equals("I")) {
                    math += 1;
                }
                if (rnArray[i].equals("V")) {
                    math += 5;
                }
                if (rnArray[i].equals("X")) {
                    math += 10;
                }
                if (rnArray[i].equals("L")) {
                    math += 50;
                }
                if (rnArray[i].equals("C")) {
                    math += 100;
                }
                if (rnArray[i].equals("D")) {
                    math += 500;
                }
                if (rnArray[i].equals("M")) {
                    math += 1000;
                }
            } else {
                if (rnArray[i].equals("I")) {
                    if (!rnArray[i + 1].equals("I")) {
                        math -= 1;
                    } else {
                        math += 1;
                    }
                }
                if (rnArray[i].equals("V")) {
                    math += 5;
                }
                if (rnArray[i].equals("X")) {
                    if (!rnArray[i + 1].matches("[IVX]")) {
                        math -= 10;
                    } else {
                        math += 10;
                    }
                }
                if (rnArray[i].equals("L")) {
                    math += 50;
                }
                if (rnArray[i].equals("C")) {
                    if (rnArray[i + 1].matches("[DM]")) {
                        math -= 100;
                    } else {
                        math += 100;
                    }
                }
                if (rnArray[i].equals("D")) {
                    math += 500;
                }
                if (rnArray[i].equals("M")) {
                    math += 1000;
                }
            }
           // System.out.println(math);
        }
        return math;
    }
}

/*If you want to try and look at the index you can uses this method

    int indexI = myIndexOf(rnArray, "I");
    int indexV = myIndexOf(rnArray, "V");
    int indexX = myIndexOf(rnArray, "X");


    public static Integer myIndexOf(String[] myArray, String myChar){
        for (int i=0;i<myArray.length;i++){

            if (myArray[i].equals(myChar) && myChar.equals("I")){
                return i;
            } else if (myArray[i].equals(myChar) && myChar.equals("V")){
                return i;
            } else if (myArray[i].equals(myChar) && myChar.equals("X")) {
                return i;
            }
        }

        return -10;
    }
}
*/

