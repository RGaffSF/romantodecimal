import com.mypackage.RomanNumerals;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class RomanNumeralsTest {

    @Test
    public void convertIto1() {
        assertEquals(1, RomanNumerals.romanNumeralConverter("I"));
    }

    @Test
    public void convertVto5() {
        assertEquals(5, RomanNumerals.romanNumeralConverter("V"));
    }

    @Test
    public void convertAllTheRomanNumerals() {
        assertEquals(1, RomanNumerals.romanNumeralConverter("I"));
        assertEquals(5, RomanNumerals.romanNumeralConverter("V"));
        assertEquals(10, RomanNumerals.romanNumeralConverter("X"));
        assertEquals(50, RomanNumerals.romanNumeralConverter("L"));
        assertEquals(100, RomanNumerals.romanNumeralConverter("C"));
        assertEquals(500, RomanNumerals.romanNumeralConverter("D"));
        assertEquals(1000, RomanNumerals.romanNumeralConverter("M"));

    }
    @Test public void passingMoreThanOneI(){
        assertEquals(1, RomanNumerals.romanNumeralConverter("I"));
        assertEquals(2, RomanNumerals.romanNumeralConverter("II"));
        assertEquals(3, RomanNumerals.romanNumeralConverter("III"));
    }

    @Test public void passingAnIandV(){
        assertEquals(6, RomanNumerals.romanNumeralConverter("VI"));
        assertEquals(8, RomanNumerals.romanNumeralConverter("VIII"));
    }

    @Test public void passingAnXandIandV(){
        assertEquals(11, RomanNumerals.romanNumeralConverter("XI"));
        assertEquals(18, RomanNumerals.romanNumeralConverter("XVIII"));
    }

    @Test public void passingAnyNumberToAdd(){
        assertEquals(21, RomanNumerals.romanNumeralConverter("XXI"));
        assertEquals(36, RomanNumerals.romanNumeralConverter("XXXVI"));
        assertEquals(52, RomanNumerals.romanNumeralConverter("LII"));
        assertEquals(525, RomanNumerals.romanNumeralConverter("DXXV"));
    }

    @Test public void minusASingleI(){
        assertEquals(9,RomanNumerals.romanNumeralConverter("IX"));
        assertEquals(99,RomanNumerals.romanNumeralConverter("IC"));
    }

    @Test public void minusAnIandX(){
        assertEquals(45,RomanNumerals.romanNumeralConverter("XLV"));
        assertEquals(44,RomanNumerals.romanNumeralConverter("XLIV"));
        assertEquals(90,RomanNumerals.romanNumeralConverter("XC"));
        assertEquals(192,RomanNumerals.romanNumeralConverter("CXCII"));
    }

    @Test public void minusC(){
        assertEquals(950,RomanNumerals.romanNumeralConverter("CML"));
        assertEquals(922,RomanNumerals.romanNumeralConverter("CMXXII"));
    }

    @Test public void randomTests() {
        assertEquals(69,RomanNumerals.romanNumeralConverter("LXIX"));
    }
}
